# LOFAR ontologies

This project hosts the ontology definitions for the LOFAR data formats. There can be used in specifying reproducible pipelines in CWL, like in https://git.astron.nl/eosc/lofar-cwl

<img src="https://www.egi.eu/wp-content/uploads/2020/01/eu-logo.jpeg" alt="EU Flag" width="80">
<img src="https://www.egi.eu/wp-content/uploads/2020/01/eosc-hub-v-web.png" alt="EOSC-hub logo" height="60">
This work is co-funded by the EOSC-hub project (Horizon 2020) under Grant number 777536.
